package com.example.githublister.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.githublister.model.Repo


interface Repository {
    fun getRepos(): LiveData<List<Repo>>
    fun getRepoDetails(owner: String, reponame: String): MutableLiveData<Repo>
}

