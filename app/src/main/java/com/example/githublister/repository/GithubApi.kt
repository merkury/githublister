package com.example.githublister.repository

import com.example.githublister.model.PopularRepos
import com.example.githublister.model.Repo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubApi {
    @GET("search/repositories")
    fun getRepos(@Query("q") query: String): Call<PopularRepos>

    @GET("repos/{owner}/{reponame}")
    fun getRepoDetails(@Path("owner") owner: String, @Path("reponame") reponame: String): Call<Repo>
}