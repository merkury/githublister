package com.example.githublister.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.example.githublister.app.Injection
import com.example.githublister.model.PopularRepos
import com.example.githublister.model.Repo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object RemoteRepository: Repository {

    private val TAG = RemoteRepository::class.java.simpleName

    private val api = Injection.provideGithubApi()

    private val liveRepo =  MutableLiveData<Repo>()

    override fun getRepos(): LiveData<List<Repo>> {

        val liveRepos = MutableLiveData<List<Repo>>()
        api.getRepos("stars:>=50000").enqueue(object : Callback<PopularRepos> {
            override fun onResponse(call: Call<PopularRepos>, response: Response<PopularRepos>?) {
                Log.d(TAG, "response: ${response?.body().toString()}")
                if (response != null) {
                    liveRepos.value = response.body()?.items
                }
            }

            override fun onFailure(call: Call<PopularRepos>, t: Throwable) {
                Log.e(TAG, t.localizedMessage)
            }
        })
        return liveRepos
    }

    override fun getRepoDetails(owner: String, reponame: String): MutableLiveData<Repo> {

            Log.d(TAG, "getRepoDetails")
            api.getRepoDetails(owner = owner, reponame = reponame).enqueue(object : Callback<Repo> {
                override fun onResponse(call: Call<Repo>, response: Response<Repo>?) {
                    Log.d(TAG, "response 1: ${response?.body().toString()}")
                    Log.d(TAG, "response 2: ${response?.toString()}")
                    if (response != null) {
                        if (response.code() == 200) {
                            liveRepo.value = response.body()
                        } else if (response.code() == 403) {
                            val lrepo = liveRepo.value
                            if (lrepo != null) {
                                val description = "${lrepo.description} -- Limit exceeded --"
                                liveRepo.value = Repo(lrepo.name, lrepo.starcount, lrepo.owner, lrepo.fullname, description)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<Repo>, t: Throwable) {
                    Log.e(TAG, t.localizedMessage)
                }
            })

        Log.d(TAG, "live Repo: $liveRepo")
        return liveRepo
    }
}



