package com.example.githublister.ui.repos

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.githublister.R
import com.example.githublister.model.Repo
import com.example.githublister.viewmodel.Details
import com.example.githublister.viewmodel.MyViewModelProviderFactory
import com.example.githublister.viewmodel.RepoDetailsViewModel
import kotlinx.android.synthetic.main.fragment_repo_detail.*

class RepoDetailFragment : Fragment() {
    private lateinit var repoDetailViewModel: RepoDetailsViewModel

    private var owner: String? = null
    private var reponame: String? = null
    private var repo: Repo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            owner = it.getString(ARG_OWNER)
            reponame = it.getString(ARG_REPONAME)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_repo_detail, container, false)

        repoDetailViewModel = ViewModelProviders.of(this, MyViewModelProviderFactory(activity!!.application, Details(owner ?: "", reponame ?: ""))).get(RepoDetailsViewModel::class.java)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        repoName.text = repo?.name
        starCount.text = repo?.starcount.toString()
        repoDetailDescription.text = repo?.description

        Log.d(TAG, "repoDetailsViewModel: ${repoDetailViewModel.repoDetails}")
        repoDetailViewModel.repoDetails.observe(this, Observer<Repo> { repoIt ->
            Log.d(TAG, "repo changed: ${repoIt?.starcount}")

            if (repoIt != null) {
                this.repo = repoIt
                repoName.text = repoIt.name
                starCount.text = repoIt.starcount.toString()
                repoDetailDescription.text = repoIt.description
            }
        })
    }

    fun setRepo(repo: Repo) {
        Log.d(TAG,"setRepo $repo")
        this.repo = repo
        if (::repoDetailViewModel.isInitialized) {
            repoDetailViewModel.repoDetails.value = repo
        } else {
            Log.d(TAG,"viewmodel not initialized")
        }
    }

    override fun onDetach() {
        super.onDetach()
        repoDetailViewModel.stopTimer()
        Log.d(TAG, "onDetach")
    }

    override fun onDestroy() {
        super.onDestroy()
        repoDetailViewModel.stopTimer()
        Log.d(TAG, "onDestroy")
    }

    companion object {
        const val ARG_OWNER = "owner"
        const val ARG_REPONAME = "reponame"
        private val TAG: String = RepoDetailFragment::class.java.simpleName
    }
}
