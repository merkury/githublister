package com.example.githublister.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.githublister.R
import com.example.githublister.model.Repo
import com.example.githublister.ui.repos.RepoAdapter
import com.example.githublister.ui.repos.RepoDetailFragment
import com.example.githublister.ui.repos.ReposFragment

class MainActivity : AppCompatActivity(), RepoAdapter.OnRepoClickListener {

    private val reposFragment = ReposFragment()
    private val repoDetailFragment = RepoDetailFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkConnectivity()
    }

    private fun switchToFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun checkConnectivity() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnected
        if (!isConnected) {
            Toast.makeText(this, "you are offline", Toast.LENGTH_SHORT).show()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.main_container, reposFragment)
                .commit()
        }
    }

    override fun onRepoClick(repo: Repo) {
        Log.d(TAG, "onlick in Main")

        repoDetailFragment.setRepo(repo)
        val args = Bundle()
        args.putString(RepoDetailFragment.ARG_OWNER, repo.owner.login)
        args.putString(RepoDetailFragment.ARG_REPONAME, repo.name)

        repoDetailFragment.arguments =args
        switchToFragment(repoDetailFragment)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d(TAG, "backPressed")
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}

