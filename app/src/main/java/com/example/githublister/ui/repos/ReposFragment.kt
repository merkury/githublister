package com.example.githublister.ui.repos

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.githublister.R
import com.example.githublister.model.Repo
import com.example.githublister.viewmodel.ReposViewModel
import kotlinx.android.synthetic.main.fragment_repos.*


class ReposFragment : Fragment() {

    private lateinit var reposViewModel: ReposViewModel

    private val adapter = RepoAdapter(mutableListOf(), null)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_repos, container, false)
        reposViewModel = ViewModelProviders.of(this).get(ReposViewModel::class.java)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        reposRecyclerView.layoutManager = LinearLayoutManager(context)
        reposRecyclerView.adapter = adapter

        reposViewModel.allRepos.observe(this, Observer<List<Repo>> { repos ->
            adapter.updateRepos(repos ?: emptyList())
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is RepoAdapter.OnRepoClickListener) {
            adapter.onRepoClickListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        adapter.onRepoClickListener = null
    }
}