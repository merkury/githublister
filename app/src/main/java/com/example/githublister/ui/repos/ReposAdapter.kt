package com.example.githublister.ui.repos


import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.githublister.R
import com.example.githublister.model.Repo
import kotlinx.android.synthetic.main.list_item_repo.view.*

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

class RepoAdapter(private val repos: MutableList<Repo>, var onRepoClickListener: OnRepoClickListener?) : RecyclerView.Adapter<RepoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.list_item_repo), onRepoClickListener)
    }

    override fun getItemCount() = repos.size

    fun updateRepos(repos: List<Repo>) {
        this.repos.clear()
        this.repos.addAll(repos)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(repos[position])
    }

    fun getItem(position: Int) = repos[position]

    inner class ViewHolder(itemView: View, private val onRepoClickListener: OnRepoClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            Log.d(TAG, "$adapterPosition")
            onRepoClickListener?.onRepoClick(getItem(adapterPosition))
        }

        private lateinit var repo: Repo

        fun bind(repo: Repo) {
            this.repo = repo
            itemView.repoName.text = repo.name
        }
    }

    interface OnRepoClickListener {
        fun onRepoClick(repo: Repo)
    }

    companion object {
        private val TAG = RepoAdapter::class.java.simpleName
    }
}