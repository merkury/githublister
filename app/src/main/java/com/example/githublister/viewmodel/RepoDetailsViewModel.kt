package com.example.githublister.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.util.Log
import com.example.githublister.app.Injection
import com.example.githublister.model.Repo
import java.util.*
import kotlin.concurrent.schedule

class Details(val owner: String, val reponame: String)

class RepoDetailsViewModel(application: Application, private val details: Details) : AndroidViewModel(application) {

    private val repository = Injection.provideRepository()
    private val timer = Timer("timerStart", false)
    var repoDetails : MutableLiveData<Repo>

    init {
        repoDetails  = repository.getRepoDetails(details.owner, details.reponame)
        startTimer(details)
    }

    private fun startTimer(details: Details) {
        timer.schedule(10000, 10000) {
            Log.d(TAG, "Details: ${details.reponame}")

            val liveData = repository.getRepoDetails(details.owner, details.reponame)
            Log.d(TAG, "liveData $liveData")
        }
    }

    fun stopTimer() {
        timer.cancel()
    }

    companion object {
        private val TAG: String = RepoDetailsViewModel::class.java.simpleName
    }
}

class MyViewModelProviderFactory(private val application: Application, private val details: Details) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = RepoDetailsViewModel(application, details) as T
}