package com.example.githublister.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.example.githublister.app.Injection

class ReposViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = Injection.provideRepository()
    val allRepos = repository.getRepos()
}
