package com.example.githublister.app

import com.example.githublister.BuildConfig
import com.example.githublister.repository.GithubApi
import com.example.githublister.repository.RemoteRepository
import com.example.githublister.repository.Repository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Injection {
    fun provideRepository(): Repository = RemoteRepository

    private fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .client(provideHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun provideHttpClient(): OkHttpClient = OkHttpClient.Builder().addInterceptor(provideLoggingInterceptor()).build()

    private fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BASIC
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    fun provideGithubApi(): GithubApi {
        return provideRetrofit().create(GithubApi::class.java)
    }
}