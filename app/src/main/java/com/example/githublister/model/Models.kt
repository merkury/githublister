package com.example.githublister.model

import com.google.gson.annotations.SerializedName

data class Repo(
    val name: String,
    @SerializedName("stargazers_count") val starcount: Int,
    val owner: Owner,
    @SerializedName("full_name") val fullname: String,  // owner.login/reponame
    var description: String
)

data class Owner(
    val login: String
)

data class PopularRepos(val items: List<Repo>)
